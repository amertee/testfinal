$(function(){

	/*
		TODO:
		az oldal betöltésekor lekérni a /rest/countries URL-ről az országok listáját,
		és a város hozzáadása popup <select> elemébe berakni az <option>-öket
	*/
        $.get('/rest/countries', function(response){
		$.each(response, function(index, value){
			var $option = $('<option></option>').text(value.iso);
			$('#city-country').append($option);
		})
	});

	$('body').on('click', '.add-city-button', function(){
		/*
			TODO: valamiért ez az eseménykezelő le sem fut...
		*/

                var cityName       = $('#city-name').val();
                var cityPop        = $('#city-population').val();
                var cityCountry    = $('#city-country').val();

		var cityData = {
			/*
				TODO: összeszedni az űrlap adatait
                                
			*/
			"name" : cityName,
			"population" : cityPop,
			'country.iso' : cityCountry

		};
		$.ajax({
			url: '/rest/cities',
			method : 'PUT',
                        type: "application/x-www-form-urlencoded",
			data : cityData,
			dataType : 'json',
			complete : function(){
				/*
					TODO: Be kellene csukni a popupot
				*/
                                $('.close-modal').trigger('click');
			}
		});
	});

	/*
		TODO: ország törlés gombok működjenek
                !!!    
	*/
        $('body').on('click', '#delete-country-button', function(){
                if ( confirm("Are you sure you want to delete?") ) {
                    var iso = $(this).closest('tr').attr("data-iso");
                    var isoTD = $(this).closest('tr');
                    
                    $.ajax({
                            url: '/rest/countries/' + iso,
                            method: 'DELETE',
                            success: function(){
                                $(isoTD).remove();
                            }
                    });
                };
            }
        );

	/*
		TODO: város törlés gombok működjenek
                !!!    
	*/
        $('body').on('click', '#delete-city-button', function(){
                if ( confirm("Are you sure you want to delete?") ) {
                    $(this).closest('tr').remove();

                    var id = $(this).closest('tr').attr("data-id");
                    var idTD = $(this).closest('tr');
                    
                    $.ajax({
                            url: '/rest/cities/' + id,
                            method: 'DELETE',
                            success: function(){
                                $(idTD).remove();
                            }
                    });
                };
            }
        );

});